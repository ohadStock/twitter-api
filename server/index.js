const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const url = require('url');
const querystring = require('querystring'); 
const twitterApiGateway = require('./twitter-api');

const app = express()

app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));

app.use(bodyParser.json({ type: 'application/json' }));
 

app.use(express.static(path.join(__dirname,'../client/')));

app.get('/api/v1/persistent', (req, res) => res.send('I am a live!'));

app.post('/api/v1/twitter/search',(req,res) => {
    const params = {
      result_type: req.body.result_type,
      count: 20
    };

    if(req.body.hashtag){
      if(req.body.hashtag[0] !== '#'){
        req.body.hashtag = `#${req.body.hashtag}` 
      }
      params.q = req.body.hashtag;
    }

    if(req.body.lang){
      params.lang = req.body.lang
    }

    if(req.body.to){
      params.until = req.body.to
    }
    
    twitterApiGateway.client.get('search/tweets', params, function(error, tweets, response) {
        res.json(tweets);
        res.end();
    });
  
})

app.listen(3000, () => console.log('Example app listening on port 3000!'))