class Api {
	
	constructor(config, emitter) {
		this.emitter = emitter;
		this.config = config;
	}

	init(){
		this.emitter.subscribe('search',this.fetch.bind(this));

		this.emitter.subscribe('display-twit', this.displayTwit.bind(this));
	}
	
	displayTwit(status) {
		const statusId = status.id;
		document.querySelector('.embedded-twit').innerHTML = '';

		twttr.widgets.createTweet(
			statusId,
			document.querySelector('.embedded-twit'),
			{
				width: '250'
			}
		);
	}

	fetch(params) {
		fetch(`${this.config.path}/search`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json; charset=utf-8",
			},
			body: JSON.stringify({...params,result_type: 'recent'})

		})
		.then(response => response.json())
		.then(data => {
			emitter.publish('fetch', data);	
		});
	}
}