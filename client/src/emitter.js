class Emitter {
	constructor() {
		this.events = new Map();
	}

	publish(type, value){
		if(this.events.has(type)){
			this.events.get(type).forEach( callback => callback(value))
		}
	}

	subscribe(type, callback){
		if(!this.events.has(type)){
			this.events.set(type,[]);
		}

		this.events.get(type).push(callback);
	}
}