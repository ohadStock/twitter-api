class App {
	
	constructor(emitter) {
		this.emitter = emitter;	
	}

	init() {
		document.querySelector('#search').addEventListener('click', this.search.bind(this));
		
		this.initTwittClick();

		this.emitter.subscribe('fetch', this.showResults.bind(this));

		this.emitter.subscribe('message', this.showMessage.bind(this));	
		
		this.setTimeLimit();
	}

	setTimeLimit(){
		document.querySelector('#from').min = this.getDateWeekAgo();
		document.querySelector('#to').min = this.getDateWeekAgo();
		document.querySelector('#from').max = this.formatDate(new Date());
		document.querySelector('#to').max = this.formatDate(new Date());
	}

	initTwittClick() {
		const parent = document.querySelector('.grid');
		const selector = '.twit';
		
		parent.addEventListener('click', event => {
			const closest = event.target.closest(selector);
			if(closest && parent.contains(closest)){
				this.emitter.publish('display-twit',{id: closest.dataset.statusId});
			}	
		})
	}
	
	search() {
		let hashtag = document.querySelector('#hashtag').value;
		const from = document.querySelector('#from').value;
		const to = document.querySelector('#to').value;
		const lang = document.querySelector('#lang').value;
		document.querySelector('.embedded-twit').innerHTML = '';

		if(!hashtag){
			this.emitter.publish('message', { type: 'error', message: 'hashtag is empty' });
			return;
		}

		//@todo check if to is bigger then from ;
		if(new Date(from) > new Date(to)){
			this.emitter.publish('message', {type: 'error', message: 'From date is bigger then To date'});
			return;
		}

		hashtag = hashtag.replace(/\s/ig, '_');
		
		this.emitter.publish('search', {
			hashtag,
			from,
			to,
			lang
		});
	}

	showResults(result) {
		let html = '<div class="result-container">';
		
		if(result.statuses.length === 0){
			html = html.concat('there is no results for your search');
		}
		else{
			result.statuses.forEach(status => {
				html = html.concat(`
					<div class='twit' data-status-id="${status.id_str}">
						<div class='user'><a target='_blank' href='https://twitter.com/${status.user.screen_name}'>${status.user.name}</a></div>
						<div class='date'>${this.getDateString(status.created_at)}</div>
						<div class='twit-text'>${status.text}</div>
						<div class='retweets'>${status.retweet_count}</div>
				</div>`)	
			});
		}

		document.querySelector('.grid').innerHTML = html.concat('</div>');
	}

	getDateString(statusDate){
		const date = new Date(statusDate);
		
		const time = date.getHours() > 12 ?
			`${date.getHours() - 12}:${date.getMinutes()} pm` :
			`${date.getHours()}: ${date.getMinutes()} am`;

		const month = date.toLocaleString('en-us', { month: 'short' })
		
		return `${time} - ${date.getDate()} ${month} ${date.getFullYear()}`;
	}

	showMessage(message){
		const messageContainer = document.querySelector('.message');
		messageContainer.innerHTML = message.message;
		
		setTimeout( () => {
			messageContainer.innerHTML = '';
		}, 3000);
	}

	getDateWeekAgo(){
		const date = new Date();
		date.setDate(date.getDate() - 7);
		return this.formatDate(date);
	}
	
	formatDate(date){
		let month = date.getMonth() + 1;
		if (month < 10) {
			month = '0'.concat(month);
		}
		return `${date.getFullYear()}-${month}-${date.getDate()}`;
	}

}

